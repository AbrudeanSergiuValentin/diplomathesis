import random
import time
import math
import tensorflow as tf
import keras.backend.tensorflow_backend as backend
import keras
import os
import cv2
import sc2
import numpy as np
from sc2 import run_game, maps, Race, Difficulty, position, Result
from sc2.player import Bot, Computer
from sc2.ids.unit_typeid import UnitTypeId


HEADLESS = False
LOG_FILE = "results_model_vs_easy.txt"

def get_session(gpu_fraction=0.3):
	# part of the gpu memory to be allocated for training
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_fraction)
    return tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


backend.set_session(get_session())


class MementoMori(sc2.BotAI):
    def __init__(self, use_model=False, title=1):
        self.MAX_WORKERS = 50
        self.do_something_after = 0
        self.use_model = use_model
        self.title = title
        self.scouts_and_spots = {}
        self.time = 0
		self.choices = {0: self.build_scout,
                        1: self.build_zealot,
                        2: self.build_gateway,
                        3: self.build_voidray,
                        4: self.build_stalker,
                        5: self.build_worker,
                        6: self.build_assimilator,
                        7: self.build_stargate,
                        8: self.build_pylon,
                        9: self.defend_nexus,
                        10: self.attack_known_enemy_unit,
                        11: self.attack_known_enemy_structure,
                        12: self.expand,
                        13: self.do_nothing,
                        }
        self.train_data = []
		
        if self.use_model:
            print("USING MODEL!")
            self.model = keras.models.load_model("FullControlMementoMoriModel")

    async def on_step(self, iteration):
        self.time = ((self.state.game_loop/22.4) / 60)

        if iteration % 5 == 0:
            await self.distribute_workers()
        await self.scout()
        await self.intel()
        await self.do_something()

    async def build_scout(self):
        for rf in self.units(UnitTypeId.ROBOTICSFACILITY).ready.noqueue:
            if self.can_afford(UnitTypeId.OBSERVER) and self.supply_left > 0:
                await self.do(rf.train(UnitTypeId.OBSERVER))
                break
				
        if len(self.units(UnitTypeId.ROBOTICSFACILITY)) == 0:
            pylon = self.units(UnitTypeId.PYLON).ready.noqueue.random
			
            if self.units(UnitTypeId.CYBERNETICSCORE).ready.exists:
                if self.can_afford(UnitTypeId.ROBOTICSFACILITY) and not self.already_pending(UnitTypeId.ROBOTICSFACILITY):
                    await self.build(UnitTypeId.ROBOTICSFACILITY, near=pylon)

    async def build_zealot(self):
        gateways = self.units(UnitTypeId.GATEWAY).ready.noqueue
		
        if gateways.exists:
            if self.can_afford(UnitTypeId.ZEALOT):
                await self.do(random.choice(gateways).train(UnitTypeId.ZEALOT))

    async def build_gateway(self):
        pylon = self.units(UnitTypeId.PYLON).ready.noqueue.random
		
        if self.can_afford(UnitTypeId.GATEWAY) and not self.already_pending(UnitTypeId.GATEWAY):
            await self.build(UnitTypeId.GATEWAY, near=pylon.position.towards(self.game_info.map_center, 5))

    async def build_voidray(self):
        stargates = self.units(UnitTypeId.STARGATE).ready.noqueue
		
        if stargates.exists:
            if self.can_afford(UnitTypeId.VOIDRAY):
                await self.do(random.choice(stargates).train(UnitTypeId.VOIDRAY))

    async def build_stalker(self):
        pylon = self.units(UnitTypeId.PYLON).ready.noqueue.random
        gateways = self.units(UnitTypeId.GATEWAY).ready
        cybernetics_cores = self.units(UnitTypeId.CYBERNETICSCORE).ready

        if gateways.exists and cybernetics_cores.exists:
            if self.can_afford(UnitTypeId.STALKER):
                await self.do(random.choice(gateways).train(UnitTypeId.STALKER))

        if not cybernetics_cores.exists:
            if self.units(UnitTypeId.GATEWAY).ready.exists:
                if self.can_afford(UnitTypeId.CYBERNETICSCORE) and not self.already_pending(UnitTypeId.CYBERNETICSCORE):
                    await self.build(UnitTypeId.CYBERNETICSCORE, near=pylon.position.towards(self.game_info.map_center, 5))

    async def build_worker(self):
        nexuses = self.units(UnitTypeId.NEXUS).ready.noqueue
		
        if nexuses.exists:
            if self.can_afford(UnitTypeId.PROBE):
                await self.do(random.choice(nexuses).train(UnitTypeId.PROBE))

    async def build_assimilator(self):
        for nexus in self.units(UnitTypeId.NEXUS).ready:
            geysers = self.state.vespene_geyser.closer_than(15.0, nexus)
			
            for geyser in geysers:
                if not self.can_afford(UnitTypeId.ASSIMILATOR):
                    break
					
                worker = self.select_build_worker(geyser.position)
				
                if worker is None:
                    break
					
                if not self.units(UnitTypeId.ASSIMILATOR).closer_than(1.0, geyser).exists:
                    await self.do(worker.build(UnitTypeId.ASSIMILATOR, geyser))

    async def build_pylon(self):
        nexuses = self.units(UnitTypeId.NEXUS).ready
		
        if nexuses.exists:
            if self.can_afford(UnitTypeId.PYLON) and not self.already_pending(UnitTypeId.PYLON):
                await self.build(UnitTypeId.PYLON,
                                 near=self.units(UnitTypeId.NEXUS).first.position.towards(self.game_info.map_center, 5))

    async def build_stargate(self):
        cybernetics_cores = self.units(UnitTypeId.CYBERNETICSCORE)
		
        if self.units(UnitTypeId.PYLON).ready.exists:
            pylon = self.units(UnitTypeId.PYLON).ready.random
            if self.units(UnitTypeId.CYBERNETICSCORE).ready.exists:
                if self.can_afford(UnitTypeId.STARGATE) and not self.already_pending(UnitTypeId.STARGATE):
                    await self.build(UnitTypeId.STARGATE, near=pylon.position.towards(self.game_info.map_center, 5))

            if not cybernetics_cores.exists:
                if self.units(UnitTypeId.GATEWAY).ready.exists:
                    if self.can_afford(UnitTypeId.CYBERNETICSCORE) and not self.already_pending(UnitTypeId.CYBERNETICSCORE):
                        await self.build(UnitTypeId.CYBERNETICSCORE, near=pylon.position.towards(self.game_info.map_center, 5))

    async def do_nothing(self):
        wait = random.randrange(7, 100) / 100
        self.do_something_after = self.time + wait

    async def defend_nexus(self):
        if len(self.known_enemy_units) > 0:
            target = self.known_enemy_units.closest_to(random.choice(self.units(UnitTypeId.NEXUS)))
            
			for u in self.units(UnitTypeId.VOIDRAY).idle:
                await self.do(u.attack(target))
            for u in self.units(UnitTypeId.STALKER).idle:
                await self.do(u.attack(target))
            for u in self.units(UnitTypeId.ZEALOT).idle:
                await self.do(u.attack(target))

    async def attack_known_enemy_structure(self):
        if len(self.known_enemy_structures) > 0:
            target = random.choice(self.known_enemy_structures)
            
			for u in self.units(UnitTypeId.VOIDRAY).idle:
                await self.do(u.attack(target))
				
            for u in self.units(UnitTypeId.STALKER).idle:
                await self.do(u.attack(target))
				
            for u in self.units(UnitTypeId.ZEALOT).idle:
                await self.do(u.attack(target))

    async def attack_known_enemy_unit(self):
        if len(self.known_enemy_units) > 0:
            target = self.known_enemy_units.closest_to(random.choice(self.units(UnitTypeId.NEXUS)))
            
			for u in self.units(UnitTypeId.VOIDRAY).idle:
                await self.do(u.attack(target))
				
            for u in self.units(UnitTypeId.STALKER).idle:
                await self.do(u.attack(target))
				
            for u in self.units(UnitTypeId.ZEALOT).idle:
                await self.do(u.attack(target))

    async def intel(self):
        game_data = np.zeros((self.game_info.map_size[1], self.game_info.map_size[0], 3), np.uint8)

        for unit in self.units().ready:
            pos = unit.position
            cv2.circle(game_data, (int(pos[0]), int(pos[1])), int(unit.radius * 8), (255, 255, 255),
                       math.ceil(int(unit.radius * 0.5)))

        for unit in self.known_enemy_units:
            pos = unit.position
            cv2.circle(game_data, (int(pos[0]), int(pos[1])), int(unit.radius * 8), (125, 125, 125),
                       math.ceil(int(unit.radius * 0.5)))

        try:
            line_max = 50
			
            mineral_ratio = self.minerals / 1500
            if mineral_ratio > 1.0:
                mineral_ratio = 1.0

            vespene_ratio = self.vespene / 1500
            if vespene_ratio > 1.0:
                vespene_ratio = 1.0

            population_ratio = self.supply_left / self.supply_cap
            if population_ratio > 1.0:
                population_ratio = 1.0

            plausible_supply = self.supply_cap / 200.0

            worker_weight = len(self.units(UnitTypeId.PROBE)) / (self.supply_cap - self.supply_left)
            if worker_weight > 1.0:
                worker_weight = 1.0

            cv2.line(game_data, (0, 19), (int(line_max * worker_weight), 19), (250, 250, 200), 3)
            cv2.line(game_data, (0, 15), (int(line_max * plausible_supply), 15), (220, 200, 200),
                     3)  
            cv2.line(game_data, (0, 11), (int(line_max * population_ratio), 11), (150, 150, 150),
                     3)
            cv2.line(game_data, (0, 7), (int(line_max * vespene_ratio), 7), (210, 200, 0), 3)
            cv2.line(game_data, (0, 3), (int(line_max * mineral_ratio), 3), (0, 255, 25), 3)
        
		except Exception as e:
            print(str(e))

        grayed = cv2.cvtColor(game_data, cv2.COLOR_BGR2GRAY)
        self.flipped = cv2.flip(grayed, 0)

        resized = cv2.resize(self.flipped, dsize=None, fx=2, fy=2)

        if not HEADLESS:
            if self.use_model:
                cv2.imshow(str(self.title), resized)
                cv2.waitKey(1)
            else:
                cv2.imshow(str(self.title), resized)
                cv2.waitKey(1)

    async def build_workers(self):
        if (len(self.units(UnitTypeId.NEXUS)) * 16) > len(self.units(UnitTypeId.PROBE)) \
                and len(self.units(UnitTypeId.PROBE)) < self.MAX_WORKERS:
            for nexus in self.units(UnitTypeId.NEXUS).ready.noqueue:
                if self.can_afford(UnitTypeId.PROBE):
                    await self.do(nexus.train(UnitTypeId.PROBE))

    # async def build_pylons(self):
    #     if self.supply_left < 5 and not self.already_pending(UnitTypeId.PYLON):
    #         nexuses = self.units(UnitTypeId.NEXUS).ready
	#
    #         if nexuses.exists:
    #             if self.can_afford(UnitTypeId.PYLON) and not self.already_pending(UnitTypeId.PYLON):
    #                 await self.build(UnitTypeId.PYLON,
    #                                  near=self.units(UnitTypeId.NEXUS).first.position.towards(self.game_info.map_center, 5))

    async def build_assimilators(self):
        for nexus in self.units(UnitTypeId.NEXUS).ready:
            geysers = self.state.vespene_geyser.closer_than(15.0, nexus)
			
            for geyser in geysers:
                if not self.can_afford(UnitTypeId.ASSIMILATOR):
                    break
					
                worker = self.select_build_worker(geyser.position)
                if worker is None:
                    break
					
                if not self.units(UnitTypeId.ASSIMILATOR).closer_than(1.0, geyser).exists:
                    await self.do(worker.build(UnitTypeId.ASSIMILATOR, geyser))

    async def expand(self):
        try:
            if self.can_afford(UnitTypeId.NEXUS) and len(self.units(UnitTypeId.NEXUS)) < 5:
                await self.expand_now()
				
        except Exception as e:
            print(str(e))

    async def build_offensive_force_buildings(self):
        if self.units(UnitTypeId.PYLON).ready.exists:
            pylon = self.units(UnitTypeId.PYLON).ready.random

            if self.units(UnitTypeId.GATEWAY).ready.exists and not self.units(UnitTypeId.CYBERNETICSCORE):
                if self.can_afford(UnitTypeId.CYBERNETICSCORE) and not self.already_pending(UnitTypeId.CYBERNETICSCORE):
                    await self.build(UnitTypeId.CYBERNETICSCORE, near=pylon)

            elif len(self.units(UnitTypeId.GATEWAY)) < 1:  # ((self.iteration / self.ITERATIONS_PER_MINUTE)/2)
                if self.can_afford(UnitTypeId.GATEWAY) and not self.already_pending(UnitTypeId.GATEWAY):
                    await self.build(UnitTypeId.GATEWAY, near=pylon)

            if self.units(UnitTypeId.CYBERNETICSCORE).ready.exists:
                if len(self.units(UnitTypeId.ROBOTICSFACILITY)) < 1:
                    if self.can_afford(UnitTypeId.ROBOTICSFACILITY) and \
                            not self.already_pending(UnitTypeId.ROBOTICSFACILITY):
                        await self.build(UnitTypeId.ROBOTICSFACILITY, near=pylon)

            if self.units(UnitTypeId.CYBERNETICSCORE).ready.exists:
                if len(self.units(UnitTypeId.STARGATE)) < self.time:
                    if self.can_afford(UnitTypeId.STARGATE) and not self.already_pending(UnitTypeId.STARGATE):
                        await self.build(UnitTypeId.STARGATE, near=pylon)

    async def build_offensive_force(self):
        # for gateway in self.units(UnitTypeId.GATEWAY).ready.noqueue:
        #     if not self.units(UnitTypeId.STALKER).amount > self.units(UnitTypeId.VOIDRAY).amount:
        #
        #         if self.can_afford(UnitTypeId.STALKER) and self.supply_left > 0:
        #             await self.do(gateway.train(UnitTypeId.STALKER))

        for stargate in self.units(UnitTypeId.STARGATE).ready.noqueue:
            if self.can_afford(UnitTypeId.VOIDRAY) and self.supply_left > 0:
                await self.do(stargate.train(UnitTypeId.VOIDRAY))

    async def attack(self):
        if len(self.units(UnitTypeId.VOIDRAY).idle) > 0:
            target = False
            if self.time > self.do_something_after:
                if self.use_model:
                    prediction = self.model.predict([self.flipped.reshape([-1, 176, 200, 3])])
                    choice = np.argmax(prediction[0])

                    choice_dict = {0: "No Attack!",
                                   1: "Attack close to our nexus!",
                                   2: "Attack Enemy Structure!",
                                   3: "Attack Eneemy Start!"}

                    print("Choice #{}:{}".format(choice, choice_dict[choice]))

                else:
                    choice = random.randrange(0, 4)

                if choice == 0:
                    # no attack
                    wait = random.randrange(7, 100) / 100
                    self.do_something_after = self.time + wait

                elif choice == 1:
                    # attack_unit_closest_nexus
                    if len(self.known_enemy_units) > 0:
                        target = self.known_enemy_units.closest_to(random.choice(self.units(UnitTypeId.NEXUS)))

                elif choice == 2:
                    # attack enemy structures
                    if len(self.known_enemy_structures) > 0:
                        target = random.choice(self.known_enemy_structures)

                elif choice == 3:
                    # attack_enemy_start
                    target = self.enemy_start_locations[0]

                if target:
                    for vr in self.units(UnitTypeId.VOIDRAY).idle:
                        await self.do(vr.attack(target))

                y = np.zeros(4)
                y[choice] = 1
                self.train_data.append([y, self.flipped])

    def find_target(self, state):
        if len(self.known_enemy_units) > 0:
            return random.choice(self.known_enemy_units)
        elif len(self.known_enemy_structures) > 0:
            return random.choice(self.known_enemy_structures)
        else:
            return self.enemy_start_locations[0]

    async def scout(self):
        self.expand_dis_dir = {}

        for el in self.expansion_locations:
            distance_to_enemy_start = el.distance_to(self.enemy_start_locations[0])
            self.expand_dis_dir[distance_to_enemy_start] = el

        self.ordered_exp_distances = sorted(k for k in self.expand_dis_dir)

        existing_ids = [unit.tag for unit in self.units]
        to_be_removed = []
        for noted_scout in self.scouts_and_spots:
            if noted_scout not in existing_ids:
                to_be_removed.append(noted_scout)

        for scout in to_be_removed:
            del self.scouts_and_spots[scout]

        if len(self.units(UnitTypeId.ROBOTICSFACILITY).ready) == 0:
            unit_type = UnitTypeId.PROBE
            unit_limit = 1
        else:
            unit_type = UnitTypeId.OBSERVER
            unit_limit = 15

        assign_scout = True

        if unit_type == UnitTypeId.PROBE:
            for unit in self.units(UnitTypeId.PROBE):
                if unit.tag in self.scouts_and_spots:
                    assign_scout = False

        if assign_scout:
            if len(self.units(unit_type).idle) > 0:
                for obs in self.units(unit_type).idle[:unit_limit]:
                    if obs.tag not in self.scouts_and_spots:
                        for dist in self.ordered_exp_distances:
                            try:
                                location = next(value for key, value in self.expand_dis_dir.items() if key == dist)
                                active_locations = [self.scouts_and_spots[k] for k in self.scouts_and_spots]
                                # self.expand_dis_dir[dist]

                                if location not in active_locations:
                                    if unit_type == UnitTypeId.PROBE:
                                        for unit in self.units(UnitTypeId.PROBE):
                                            if unit.tag in self.scouts_and_spots:
                                                continue

                                    await self.do(obs.move(location))
                                    self.scouts_and_spots[obs.tag] = location
                                    break
									
                            except Exception as e:
                                pass

        for obs in self.units(unit_type):
            if obs.tag in self.scouts_and_spots:
                if obs in [probe for probe in self.units(UnitTypeId.PROBE)]:
                    await self.do(obs.move(self.random_location_variance(self.scouts_and_spots[obs.tag])))

    def random_location_variance(self, location):
        x = location[0]
        y = location[1]

        x += random.randrange(-5, 5)
        y += random.randrange(-5, 5)

        if x < 0:
            print("x below")
            x = 0
			
        if y < 0:
            print("y below")
            y = 0
			
        if x > self.game_info.map_size[0]:
            print("x above")
            x = self.game_info.map_size[0]
			
        if y > self.game_info.map_size[1]:
            print("y above")
            y = self.game_info.map_size[1]

        go_to = position.Point2(position.Pointlike((x, y)))

        return go_to

    def on_end(self, game_result):
        print('--- on_end called ---')
        print(game_result, self.use_model)

        if self.use_model:
            with open(LOG_FILE, "a") as f:
                f.write("Model {} - {}\n".format(game_result, 
								time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(time.time())))))				

        if game_result == Result.Victory:
            np.save("train_data/{}.npy".format(str(int(time.time()))), np.array(self.train_data))

    async def do_something(self):

        the_choices = {0: "build_scout",
                       1: "build_zealot",
                       2: "build_gateway",
                       3: "build_voidray",
                       4: "build_stalker",
                       5: "build_worker",
                       6: "build_assimilator",
                       7: "build_stargate",
                       8: "build_pylon",
                       9: "defend_nexus",
                       10: "attack_known_enemy_unit",
                       11: "attack_known_enemy_structure",
                       12: "expand",
                       13: "do_nothing",
                       }

        if self.time > self.do_something_after:
            if self.use_model:
                worker_weight = 1.5
                zealot_weight = 1.1
                voidray_weight = 1
                stalker_weight = 1.4
                pylon_weight = 1.3
                stargate_weight = 1
                gateway_weight = 1.3
                assimilator_weight = 1.7

                prediction = self.model.predict([self.flipped.reshape([-1, 176, 200, 1])])
                weights = [1, zealot_weight, gateway_weight, voidray_weight, stalker_weight, worker_weight,
                           assimilator_weight, stargate_weight, pylon_weight, 1, 1, 1, 1, 1]
                weighted_prediction = prediction[0] * weights
                choice = np.argmax(weighted_prediction)
                print('Choice:', the_choices[choice])
            else:
                worker_weight = 8
                zealot_weight = 3
                voidray_weight = 20
                stalker_weight = 8
                pylon_weight = 5
                stargate_weight = 5
                gateway_weight = 3

                choice_weights = 1 * [0] + zealot_weight * [1] + gateway_weight * [2] + voidray_weight * [
                    3] + stalker_weight * [4] + worker_weight * [5] + 1 * [6] + stargate_weight * [7] + pylon_weight * [
                                     8] + 1 * [9] + 1 * [10] + 1 * [11] + 1 * [12] + 1 * [13]
                choice = random.choice(choice_weights)

            try:
                await self.choices[choice]()
            except Exception as e:
                print(str(e))

            y = np.zeros(14)
            y[choice] = 1
            self.train_data.append([y, self.flipped])


while True:
    run_game(maps.get("Abyssal Reef LE"), [
        Bot(Race.Protoss, MementoMori(True)),
        # Bot(Race.Protoss, MementoMori(use_model=False, title=1)),
        Computer(Race.Protoss, Difficulty.Easy)
    ], realtime=False)
